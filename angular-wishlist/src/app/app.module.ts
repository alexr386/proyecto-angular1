import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppPrueba2Component } from './app-prueba2/app-prueba2.component';
import { NuevaListaComponent } from './nueva-lista/nueva-lista.component';

@NgModule({
  declarations: [
    AppComponent,
    AppPrueba2Component,
    NuevaListaComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
