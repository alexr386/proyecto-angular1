import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppPrueba2Component } from './app-prueba2.component';

describe('AppPrueba2Component', () => {
  let component: AppPrueba2Component;
  let fixture: ComponentFixture<AppPrueba2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppPrueba2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppPrueba2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
