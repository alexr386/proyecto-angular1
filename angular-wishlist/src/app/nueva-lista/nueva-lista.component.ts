import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nueva-lista',
  templateUrl: './nueva-lista.component.html',
  styleUrls: ['./nueva-lista.component.css']
})
export class NuevaListaComponent implements OnInit {
  lista:string[]
  constructor() {
    this.lista = ['Numero 1', 'Numero 2', 'Numero 3', 'Numero 4'];
   }

  ngOnInit(): void {
  }

}
